import { Component, OnInit,EventEmitter,Output} from '@angular/core';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  @Output() recipeWasSelected = new EventEmitter<Recipe>();

  recipes: Recipe[] = [
    new Recipe("One Recipe","Desc Recipe","https://image.shutterstock.com/display_pic_with_logo/438409/540997561/stock-photo-selection-of-healthy-food-540997561.jpg"),
    new Recipe("Two Recipe","Desc Recipe","https://image.shutterstock.com/display_pic_with_logo/438409/540997561/stock-photo-selection-of-healthy-food-540997561.jpg"),
  ]
    constructor() {

   }

  ngOnInit() {
  }

  onRecipeSelected(recipe : Recipe){
    this.recipeWasSelected.emit(recipe);    
  }
}
